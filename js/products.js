let products_list = [['Breeze ActivBleach Powder Pack', "all_extracted_patches/Breeze ActivBleach Powder Pack/160022638157375d7f350-f7cb-11ea-b008-7d3e8a75d24b_3_2.jpg"],
['Breeze Antibacterial Powder Pack', "all_extracted_patches/Breeze Antibacterial Powder Pack/160584582994442155b80-2ae7-11eb-aadd-2d43f053f716_20_4.jpg"],
['Breeze PowerMachine Liquid Pack', "all_extracted_patches/Breeze PowerMachine Liquid Pack/160584582994442155b80-2ae7-11eb-aadd-2d43f053f716_20_1.jpg"],
['Breeze Rose Gold Perfume Powder Pack', "all_extracted_patches/Breeze Rose Gold Perfume Powder Pack/160584582994442155b80-2ae7-11eb-aadd-2d43f053f716_20_0.jpg"],
['Clear Antidandruff Sachet', "all_extracted_patches/Clear Antidandruff Sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_3_1.jpg"],
['Clear Coolsport menthol sachet', "all_extracted_patches/Clear Coolsport menthol sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_2_0.jpg"],
['Clear Nourished and healthy sachet', "all_extracted_patches/Clear Nourished and healthy sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_4_0.jpg"],
['Comfort Detergent Liquid Casual Care Pack', "all_extracted_patches/Comfort Detergent Liquid Casual Care Pack/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_37_4.jpg"],
['Comfort Detergent Liquid Glamour Care Pack', "all_extracted_patches/Comfort Detergent Liquid Glamour Care Pack/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_39_0.jpg"],
['Comfort Detergent Powder Glamour Care Pack', "all_extracted_patches/Comfort Detergent Powder Glamour Care Pack/733e1280-f381-11ea-83b4-2d271c983c35_2_0.jpg"],
['Cream Silk Damage Control Sachet', "all_extracted_patches/Cream Silk Damage Control Sachet/16056740666725731ff00-2957-11eb-b758-b3d94e6c147e_6_1.jpg"],
['Cream Silk Dry Rescue Sachet', "all_extracted_patches/Cream Silk Dry Rescue Sachet/16056740666725731ff00-2957-11eb-b758-b3d94e6c147e_9_3.jpg"],
['Cream Silk Hairfall  Defence Sachet', "all_extracted_patches/Cream Silk Hairfall  Defence Sachet/545723-e0d5-fdf8-a38c-36a36728bbf_0_0.jpg"],
['Cream Silk Standout Sachet', "all_extracted_patches/Cream Silk Standout Sachet/16056740666725731ff00-2957-11eb-b758-b3d94e6c147e_3_1.jpg"],
['Cream Silk Stunning Shine Sachet', "all_extracted_patches/Cream Silk Stunning Shine Sachet/16056740666725731ff00-2957-11eb-b758-b3d94e6c147e_6_3.jpg"],
['Cream Silk Ultimate Color Sachet', "all_extracted_patches/Cream Silk Ultimate Color Sachet/1605674728596e1bb9540-2958-11eb-b758-b3d94e6c147e_3_0.jpg"],
['Cream Silk Ultimate Repair Treatment Wrap', "all_extracted_patches/Cream Silk Ultimate Repair Treatment Wrap/1600059445441c827b310-f646-11ea-a2ca-7d726414a36a_3_0.jpg"],
['Cream Soup Mushroom', "all_extracted_patches/Cream Soup Mushroom/f2b7011-8e00-dbad-7328-7f8ac16ba036_9_0.jpg"],
['Dove 1Min Hairfall Rescue Sachet', "all_extracted_patches/Dove 1Min Hairfall Rescue Sachet/1600225684277d6390650-f7c9-11ea-b008-7d3e8a75d24b_2_1.jpg"],
['Dove 1Min Hairfall Rescue Tube', "all_extracted_patches/Dove 1Min Hairfall Rescue Tube/4a8bb0-1e17-82-7657-56b4b8da17df_1_2.jpg"],
['Dove 1Min Intense Repair Tube', "all_extracted_patches/Dove 1Min Intense Repair Tube/4a8bb0-1e17-82-7657-56b4b8da17df_1_1.jpg"],
['Dove 1Min Nourishing Oil Care Tube', "all_extracted_patches/Dove 1Min Nourishing Oil Care Tube/4a8bb0-1e17-82-7657-56b4b8da17df_1_3.jpg"],
['Dove 1Min Nourishing Sachet', "all_extracted_patches/Dove 1Min Nourishing Sachet/1600225684277d6390650-f7c9-11ea-b008-7d3e8a75d24b_2_0.jpg"],
['Dove Intense Repair Sachet', "all_extracted_patches/Dove Intense Repair Sachet/1605674728596e1bb9540-2958-11eb-b758-b3d94e6c147e_11_0.jpg"],
['Dove Nourishing Oil Bottle', "all_extracted_patches/Dove Nourishing Oil Bottle/e4e8d6-1f84-8ddf-8715-04746dbcb4a_10_0.jpg"],
['Dove Nourishing Oil Care Bottle', "all_extracted_patches/Dove Nourishing Oil Care Bottle/4a8bb0-1e17-82-7657-56b4b8da17df_1_4.jpg"],
['Dove Nourishing Oil Sachet', "all_extracted_patches/Dove Nourishing Oil Sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_11_1.jpg"],
['Dove Oxygen Bottle', "all_extracted_patches/Dove Oxygen Bottle/e4e8d6-1f84-8ddf-8715-04746dbcb4a_10_2.jpg"],
['Dove Soothing Moisture Bottle', "all_extracted_patches/Dove Soothing Moisture Bottle/e4e8d6-1f84-8ddf-8715-04746dbcb4a_4_1.jpg"],
['Dove Straight and Silky Sachet', "all_extracted_patches/Dove Straight and Silky Sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_10_1.jpg"],
['Hot Meals Arroz Caldo Mix', "all_extracted_patches/Hot Meals Arroz Caldo Mix/f2b7011-8e00-dbad-7328-7f8ac16ba036_6_3.jpg"],
['Liquid Seasoning Original', "all_extracted_patches/Liquid Seasoning Original/16061872666233a33fcf0-2e02-11eb-b4c4-29d744d49a24_1_4.jpg"],
['Real Soup Crab and Corn', "all_extracted_patches/Real Soup Crab and Corn/113e77a-2f5-8a14-22aa-645551faf43a_2_1.jpg"],
['Recipe Mix Ginataang Gulay Sachet', "all_extracted_patches/Recipe Mix Ginataang Gulay Sachet/16061871744510343a830-2e02-11eb-b4c4-29d744d49a24_2_1.jpg"],
['Sinigang sa Sampalok Mix Original', "all_extracted_patches/Sinigang sa Sampalok Mix Original/1605683794696fd8ca880-296d-11eb-b758-b3d94e6c147e_1_2.jpg"],
['Sunsilk Coconut Sachet', "all_extracted_patches/Sunsilk Coconut Sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_18_0.jpg"],
['Sunsilk DR Sachet', "all_extracted_patches/Sunsilk DR Sachet/1600055904009894c0f90-f63e-11ea-a2ca-7d726414a36a_7_1.jpg"],
['Sunsilk Smooth Bottle', "all_extracted_patches/Sunsilk Smooth Bottle/1600055904009894c0f90-f63e-11ea-a2ca-7d726414a36a_9_3.jpg"],
['Sunsilk S&M Sachet', "all_extracted_patches/Sunsilk S&M Sachet/1600055904009894c0f90-f63e-11ea-a2ca-7d726414a36a_15_0.jpg"],
['Sunsilk Strong Bottle', "all_extracted_patches/Sunsilk Strong Bottle/1600055904009894c0f90-f63e-11ea-a2ca-7d726414a36a_9_2.jpg"],
['Sunsilk Strong Sachet', "all_extracted_patches/Sunsilk Strong Sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_14_0.jpg"],
['Sunsilk Watermelon Sachet', "all_extracted_patches/Sunsilk Watermelon Sachet/1600055904009894c0f90-f63e-11ea-a2ca-7d726414a36a_6_0.jpg"],
['Sunslik Expert Perfect Sachet', "all_extracted_patches/Sunslik Expert Perfect Sachet/1605669977486d1d9e1f0-294d-11eb-b758-b3d94e6c147e_20_1.jpg"],
['Surf Active Clean Cherry Blossom Pack', "all_extracted_patches/Surf Active Clean Cherry Blossom Pack/1606190560438e5786c70-2e09-11eb-b4c4-29d744d49a24_2_0.jpg"],
['Surf Active Clean Kalamansi Pack', "all_extracted_patches/Surf Active Clean Kalamansi Pack/1600748086559263f4af0-fc8a-11ea-8a31-3d2efd012eb8_4_0.jpg"],
['Surf Active Clean Purple Blooms Pack', "all_extracted_patches/Surf Active Clean Purple Blooms Pack/1600748086559263f4af0-fc8a-11ea-8a31-3d2efd012eb8_7_0.jpg"],
['Surf Active Clean Sun Fresh Pack', "all_extracted_patches/Surf Active Clean Sun Fresh Pack/1606190560438e5786c70-2e09-11eb-b4c4-29d744d49a24_6_0.jpg"],
['Surf Blossom Fresh Pack', "all_extracted_patches/Surf Blossom Fresh Pack/1600748086559263f4af0-fc8a-11ea-8a31-3d2efd012eb8_1_0.jpg"],
['Surf Fabric Conditioner Blossom Fresh Pack', "all_extracted_patches/Surf Fabric Conditioner Blossom Fresh Pack/88bf28-73b2-fd4-2308-0a2ad41af_8_4.jpg"],
['Surf Fabric Conditioner Fabcon Blossom Fresh', "all_extracted_patches/Surf Fabric Conditioner Fabcon Blossom Fresh/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_18_1.jpg"],
['Surf Fabric Conditioner Fabcon Cherry Blossom', "all_extracted_patches/Surf Fabric Conditioner Fabcon Cherry Blossom/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_24_0.jpg"],
['Surf Fabric Conditioner Fabcon PurpleBlooms', "all_extracted_patches/Surf Fabric Conditioner Fabcon PurpleBlooms/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_30_1.jpg"],
['Surf Fabric Conditioner Fabcon Sun Fresh', "all_extracted_patches/Surf Fabric Conditioner Fabcon Sun Fresh/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_27_1.jpg"],
['Surf Fabric Conditioner Kalamansi', "all_extracted_patches/Surf Fabric Conditioner Kalamansi/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_33_3.jpg"],
['Surf Fabric Conditioner Luxe Perfume Pack', "all_extracted_patches/Surf Fabric Conditioner Luxe Perfume Pack/35e6e77-d838-3485-5fe5-83ca0f1b8e6_4_0.jpg"],
['Surf Fabric Conditioner Magical Bloom Pack', "all_extracted_patches/Surf Fabric Conditioner Magical Bloom Pack/074175-cd81-410-4a62-f73e86c4b16_8_0.jpg"],
['Surf Fabric Conditioner Tawas', "all_extracted_patches/Surf Fabric Conditioner Tawas/1600070126085a650a350-f65f-11ea-a2ca-7d726414a36a_36_3.jpg"],
['Surf Fabric Conditioner With Mint Extracts Bottle', "all_extracted_patches/Surf Fabric Conditioner With Mint Extracts Bottle/88bf28-73b2-fd4-2308-0a2ad41af_7_0.jpg"],
['Surf Fabric Conditioner With Mint Extracts Pack', "all_extracted_patches/Surf Fabric Conditioner With Mint Extracts Pack/88bf28-73b2-fd4-2308-0a2ad41af_7_1.jpg"],
['Tresemme Color Radiance Bottle', "all_extracted_patches/Tresemme Color Radiance Bottle/1600059445441c827b310-f646-11ea-a2ca-7d726414a36a_25_4.jpg"],
['Vitakeratin Deep Repair Sachet', "all_extracted_patches/Vitakeratin Deep Repair Sachet/1605672043744a16fae00-2952-11eb-b758-b3d94e6c147e_6_2.jpg"]];